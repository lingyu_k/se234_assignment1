import de.saxsys.javafx.test.JfxRunner;
import javafx.scene.input.KeyCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import platformer.model.Character;

@RunWith(JfxRunner.class)
public class CharacterTest {
    private Character floatingCharacter;
    private int x = 30;
    private int y = 30;
    private int offsetX = 0;
    private int offsetY = 0;
    private KeyCode leftKey = KeyCode.A;
    private KeyCode rightKey = KeyCode.D;
    private KeyCode upKey = KeyCode.W;
    @Before
    public void setUp() {
        floatingCharacter = new Character(30, 30, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W);
    }
    @Test
    public void whenConstructorIsCalledThenCharacterFieldsShouldMatchConstructorArguments() {
        assertEquals(x, floatingCharacter.getX(), 0.0);
        assertEquals(y, floatingCharacter.getY(), 0.0);
        assertEquals(offsetX, floatingCharacter.getOffsetX(), 0.0);
        assertEquals(offsetY, floatingCharacter.getOffsetY(), 0.0);
        assertEquals(leftKey, floatingCharacter.getLeftKey());
        assertEquals(rightKey, floatingCharacter.getRightKey());
        assertEquals(upKey, floatingCharacter.getUpKey());
    }






}