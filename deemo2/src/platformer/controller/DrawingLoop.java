package platformer.controller;

import platformer.model.Character;
import platformer.model.Character2;
import platformer.model.Enemy;
import platformer.model.Stone;
import platformer.view.Platform;

import java.util.ArrayList;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(Character character, Character2 character2, ArrayList<Enemy> enemyList, Stone stone) throws InterruptedException {

            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
            character.checkOnTheStage();
        character2.checkReachGameWall();
        character2.checkReachHighest();
        character2.checkReachFloor();
        character2.checkOnTheStage();


        for (Enemy e : enemyList) {
            if (character.getBoundsInParent().intersects(e.getBoundsInParent())) {
                character.collided2(e);
            }
            if (character2.getBoundsInParent().intersects(e.getBoundsInParent())) {
                character2.collided2(e);
            }


        }





                if( character.getScore() != character2.getScore2()) {
                    if (character.getBoundsInParent().intersects(character2.getBoundsInParent())) {
                        character.collided(character2,stone);
                        character2.collided(character,stone);

                        return;
                    }
                }
            }







    private void paint(Character character, Character2 character2, ArrayList<Enemy> enemyList,Stone stone) throws InterruptedException {
        character.repaint();
        character2.repaint();
        stone.repaint();
    }

    @Override
    public void run() {
        while (running) {
            try {
                checkDrawCollisions(platform.getMainCharacter(),platform.getMainCharacter2(),platform.getEnemyList(),platform.getStone());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            float time = System.currentTimeMillis();

            try {
                checkDrawCollisions(platform.getMainCharacter(),platform.getMainCharacter2(),platform.getEnemyList(),platform.getStone());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                paint(platform.getMainCharacter(),platform.getMainCharacter2(),platform.getEnemyList(),platform.getStone());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }


}
