package platformer;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import platformer.controller.DrawingLoop;
import platformer.controller.GameLoop;
import platformer.view.Platform;

public class Main extends Application {

    private boolean inMenu = true;
    private boolean notmenu = true;
    private boolean running = true;
    public boolean introFinnised = false;
    public static boolean isSelect = false;
    static int GameMode = 4;

    private static Scene scene0;
    private static Scene scene1;
    private static Scene scene2;
    private static Scene scene3;


    static Platform menuPlatform = new Platform();
    static Platform platform = new Platform();
    static Platform introPlatform = new Platform();
    static Platform goverPlatform = new Platform();
    static GameLoop gameLoop = new GameLoop(platform);
    static DrawingLoop drawingLoop = new DrawingLoop(platform);

    public static Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage pStage) {

        pStage.setTitle("Super Mario");
        pStage.setResizable(false);
        pStage.sizeToScene();
        pStage.setAlwaysOnTop(true);

        introPlatform.runIntro();
        scene0 = new Scene(introPlatform, platform.WIDTH, platform.HEIGHT, Color.BLACK);
        pStage.setScene(scene0);

        pStage.show();
        primaryStage = pStage;

        PauseTransition pause = new PauseTransition(Duration.seconds(5));
        pause.setOnFinished(event1 -> {
                    changeGameMode(0);

                    introFinnised = true;
                    System.out.println("Loading menu");

                    while (running && introFinnised) {
                        if (GameMode == 0) {

                            menuPlatform.setMenu();
                            scene2 = new Scene(menuPlatform, platform.WIDTH, platform.HEIGHT, Color.BLACK);
                            primaryStage.setScene(scene2);

                            System.out.println("please select something");
                            Platform.startBtn.setOnMouseClicked(event -> changeGameMode(1));
                            Platform.exitBtn.setOnMouseClicked(event -> changeGameMode(2));

                            return;
                        }

                    }
                }
        );
        pause.play();

    }

    public static void changeGameMode(int j) {

        primaryStage.setScene(null);

        if (j == 1) {
            GameMode = 1;

            scene1 = new Scene(platform, platform.WIDTH, platform.HEIGHT);
            scene1.setRoot(platform);
            primaryStage.setScene(scene1);

            scene1.setOnKeyPressed(event -> platform.getKeys().add(event.getCode()));
            scene1.setOnKeyReleased(event -> platform.getKeys().remove(event.getCode()));

            javafx.application.Platform.runLater(() -> {
                (new Thread(gameLoop)).start();
                (new Thread(drawingLoop)).start();
            });

            System.out.println("Game mode change to 1");


        } else if (j == 2) {

            GameMode = 2;
            isSelect = true;

            System.out.println("Game mode change to 2");
            primaryStage.close();

        } else if (j == 0) {

            GameMode = 0;
            System.out.println("Game mode change to 0");

        } else if (GameMode == 4) {

            goverPlatform.setOver();
            scene3 = new Scene(goverPlatform, platform.WIDTH, platform.HEIGHT, Color.WHEAT);
            primaryStage.setScene(scene3);

            System.out.println("please select something");
            Platform.retry.setOnMouseClicked(event -> changeGameMode(1));
            Platform.exit2.setOnMouseClicked(event -> changeGameMode(2));

        }
    }


}
