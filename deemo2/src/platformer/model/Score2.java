package platformer.model;

import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class Score2 extends Pane {

    Label point;
    public Score2(int x, int y) {
        point = new Label("0");
        point.setStyle("-fx-background-color:blue;-fx-border-color:black;-fx-border-size:8");
        setTranslateX(x);
        setTranslateY(y);
        point.setFont(Font.font("Verdana",FontWeight.BOLD,40));
        point.setTextFill(Color.web("#FFF"));
        getChildren().addAll(point);
    }

    public void setPoint2(int score2) {
        this.point.setText(Integer.toString(score2));
    }
}