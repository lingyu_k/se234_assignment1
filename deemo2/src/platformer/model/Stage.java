package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;


public class Stage extends Pane {
    public static final int STAGE_WIDTH = 60;
    public static final int STAGE_HEIGHT = 34;
    public static int stage_x;
    public static int stage_y;
    private Image stageImg;
    private ImageView imageView;

    public Stage(int stage_x, int stage_y) {
        this.stage_x = stage_x;
        this.stage_y = stage_y;
        this.setTranslateX(stage_x);
        this.setTranslateY(stage_y);
        this.stageImg = new Image(getClass().getResourceAsStream("/Wall.png"));
        this.imageView = new ImageView(stageImg);

        this.imageView.setFitWidth(STAGE_WIDTH);
        this.imageView.setFitHeight(STAGE_HEIGHT+30);
        this.getChildren().addAll(this.imageView);
    }
}
