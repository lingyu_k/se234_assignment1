package platformer.model;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import org.apache.logging.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import platformer.view.Platform;

import java.awt.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Fireball extends Pane {

    Logger logger = LoggerFactory.getLogger(Character.class);

    public static final int CHARACTER_WIDTH = 32;
    public static final int CHARACTER_HEIGHT = 64;

    private Image characterImg;


    private AnimatedSprite imageView;

    private int x;
    private int y;
    private int startX;
    private int startY;
    private double offsetX;
    private double offsetY;

    private int score = 0;

    private KeyCode fireKey;
    private KeyCode fireKey2;
    private KeyCode rightKey;
    private KeyCode upKey;
    private Character character;


    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 5;
    int yMaxVelocity = 17;
    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    boolean isRes = false;

    public Fireball(int x, int y, double offsetX, double offsetY, Character character) {

        this.startX = x;
        this.startY = y;
        this.offsetX = x;
        this.offsetY = y;
        this.character = character;

        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/platformer/assets/Bullet.png"));

        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);


        this.getChildren().addAll(this.imageView);
    }

    public KeyCode getKey() {
        return fireKey;
    }

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }

    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    //
    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;

    }

    public void checkReachGameWall() {
        if (x <= 0) {
            x = -30;
        } else if (x + getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH;
        }
    }




    //
    public void moveX() {
        setTranslateX(x);

        if (isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity ? xMaxVelocity : xVelocity + xAcceleration;
            x = x - xVelocity;
        }
        if (isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity ? xMaxVelocity : xVelocity + xAcceleration;
            x = x + xVelocity;
        }
    }

    public void setX(int x) {
        this.x = x;
    }

    public void repaint() {
        moveX();
//        moveY();
    }


    public void collided1(Character c) throws InterruptedException {


    }

    public int getScore() {
        return score;
    }

}
