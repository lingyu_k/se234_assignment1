package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import platformer.view.Platform;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static platformer.model.Stone.ITEM_HEIGHT;
import static platformer.model.Stone.ITEM_WIDTH;

public class Character2 extends Pane {

    Logger logger = LoggerFactory.getLogger(Character.class);

    public static final int CHARACTER_WIDTH = 32;
    public static final int CHARACTER_HEIGHT = 64;

    private Image characterImg;


    private AnimatedSprite imageView;

    private int x;
    private int y;
    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;

    private int score2=0;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;

    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 7;
    int yMaxVelocity = 17;
    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    ArrayList<Enemy> toKill;
    private AnimatedSprite currentImageView;

    boolean touched = false;
    public Character2(int x, int y, int offsetX, int offsetY, KeyCode leftKey, KeyCode rightKey, KeyCode upKey) {

        this.startX = x;
        this.startY = y;
        this.offsetX = x;
        this.offsetY = y;

        this.toKill = new ArrayList<>();

        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/MarioSheet.png"));
        this.imageView = new AnimatedSprite(characterImg,4,4,offsetX,offsetY,16,32);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.getChildren().addAll(this.imageView);



    }
    public void respawn() {
        x = startX;
        y = startY;
        imageView.setFitWidth(CHARACTER_WIDTH);
        imageView.setFitHeight(CHARACTER_HEIGHT);
        isMoveLeft = false;
        isMoveRight = false;
        falling = true;
        canJump = false;
        jumping = false;
        score2--;
    }

    public void getEnemy(){
        for (Enemy e : toKill) {
            e.respawn2(this);
            score2++;
        }
        toKill.clear();


    }



    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
    }

    public void jump() {
        if (canJump) {
            yVelocity = yMaxVelocity;
            canJump = false;
            jumping = true;
            falling = false;
        }
    }

    public void checkReachHighest() {
        if(jumping &&  yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
            y = Platform.GROUND - CHARACTER_HEIGHT;
            falling = false;
            canJump = true;
            yVelocity = 0;
        }
    }

    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-CHARACTER_WIDTH;
        }


    }

    public void checkOnTheStage() {
        if (falling && y > Stage.stage_y - CHARACTER_HEIGHT && x >= Stage.stage_x - CHARACTER_WIDTH && x <= Stage.stage_x + Stage.STAGE_WIDTH) {
            y = Stage.stage_y - CHARACTER_HEIGHT;
            falling = true;
            canJump = true;
            yVelocity = 0;

        }
    }



    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x - xVelocity;
        }
        if(isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        }
        else if(jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }



    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public AnimatedSprite getImageView() { return imageView; }


    public int getX() {
        return x;
    }



    public int getY() {
        return y;
    }



    public int getScore2() {
        return score2;
    }


    public void collided(Character character2,Stone stone) throws InterruptedException {

        if (isMoveLeft) {
            x = character2.getX() + CHARACTER_WIDTH + 1;
            stop();

        } else if (isMoveRight) {
            x = character2.getX() - CHARACTER_WIDTH - 1;
            stop();

        }
        //Not on the ground
        if(y < Platform.GROUND - CHARACTER_HEIGHT) {
            //Above c
            if( falling && y < character2.getY() && Math.abs(y-character2.getY())<=CHARACTER_HEIGHT+1) {
                y = Platform.GROUND - CHARACTER_HEIGHT - 5;
                repaint();
                character2.collapsed();
                character2.respawn();
                score2--;
            }


        }
    }



    public void collapsed() throws InterruptedException {
        imageView.setFitHeight(5);
        y = Platform.GROUND - 5;
        repaint();
        TimeUnit.MILLISECONDS.sleep(500);
    }




    public double getOffsetX() {
        return 0;
    }

    public double getOffsetY() {
        return 0;
    }

    public void setCurrentImageView(AnimatedSprite currentImageView) {
        this.currentImageView = currentImageView;
    }

    public AnimatedSprite getCurrentImageView() {
        return currentImageView;
    }

    public void collided2(Enemy e) {
        if (touched=true) {
            if(!toKill.contains(e)) toKill.add(e);
            trace();
        }
    }


    public void trace() {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());


        System.out.println(String.format("Character 2 get 1 point "+"x:%d y:%d vx:%d vy:%d "+timestamp,x,y,xVelocity,yVelocity));
    }




}
