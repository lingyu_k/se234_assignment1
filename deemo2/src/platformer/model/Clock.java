package platformer.model;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.util.Duration;
public class Clock extends Pane{
    public Timeline animation;
    public String S = "";
    public boolean isClear;

    public int getTmp() {
        return tmp;
    }

    public void setTmp(int tmp) {
        this.tmp = tmp;
    }

    public int tmp = 30;
    Label label = new Label("300");
    public Clock() {
        label.setFont(Font.font("Impact", 50));
        label.setLayoutX(400);
        getChildren().add(label);
        animation = new Timeline(new KeyFrame(Duration.millis(1000), e -> timelabel()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
        this.isClear = false;
    }


    public void timelabel() {
        tmp--;
        S = tmp + "";
        label.setText(S);
        if (tmp == 0) {
            animation.stop();
            label.setText("0");
        }
    }


    public boolean CheckIsClear() {
        if (tmp == 0) {
            isClear = true;
        }
        return isClear;
    }
}
