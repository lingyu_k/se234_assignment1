package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import platformer.view.Platform;

import java.util.Random;

public class Enemy extends Pane {
    public static int ITEM_WIDTH = 32;
    public static final int ITEM_HEIGHT = 64;
    public int cur_width;
    public int cur_height;
    private int score = 0;
    private Image itemImg;
    private ImageView imageView;

    public Enemy(Character mainCharacter) {
        this.itemImg = new Image(getClass().getResourceAsStream("/Coin.png"));
        this.imageView = new ImageView(itemImg);
        this.getChildren().addAll(this.imageView);
        respawn(mainCharacter);
    }

    public void respawn(Character mainCharacter) {
        Random rn = new Random();

        int rand_x = mainCharacter.getX();
        int rand_y = mainCharacter.getY();
        while (rand_x >= mainCharacter.getX() - 10 && rand_x <= mainCharacter.getX() + 10 && rand_y >= mainCharacter.getY() - 10 && rand_y <= mainCharacter.getY() + 10) {
            rand_x = rn.nextInt(Platform.WIDTH - cur_width + 1);
            rand_y = Platform.GROUND - rn.nextInt(Platform.GROUND - 150);
            score++;
        }

        this.setTranslateX(rand_x);
        this.setTranslateY(rand_y);
    }
    public void respawn2(Character2 mainCharacter2) {
        Random rn = new Random();

        int rand_x = mainCharacter2.getX();
        int rand_y = mainCharacter2.getY();
        while (rand_x >= mainCharacter2.getX() - 10 && rand_x <= mainCharacter2.getX() + 10 && rand_y >= mainCharacter2.getY() - 10 && rand_y <= mainCharacter2.getY() + 10) {
            rand_x = rn.nextInt(Platform.WIDTH - cur_width + 1);
            rand_y = Platform.GROUND - rn.nextInt(Platform.GROUND - 100);
            score++;
        }

        this.setTranslateX(rand_x);
        this.setTranslateY(rand_y);
    }

    public ImageView getImageView() {
        return imageView;
    }
}