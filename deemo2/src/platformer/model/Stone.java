package platformer.model;

import javafx.animation.Animation;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import platformer.view.Platform;

import java.util.Random;

public class Stone extends Pane {

    private static final int OFFSET_X =  34;
    private static final int OFFSET_Y =  33;
    public static final int ITEM_WIDTH = 34;
    public static final int ITEM_HEIGHT = 33;
    private Image enemyImg;
    private ImageView imageView;
    boolean falling = true;
    boolean touched = false;
    Character toKill;
    Character2 tokill2;

    public KeyCode getFireKey() {
        return fireKey;
    }

    private KeyCode fireKey;


    public int getX() {
        return x;
    }

    private int x;

    public int getY() {
        return y;
    }

    private int y;
    private int startX;
    private int startY;


    int width;
    int height;

    int xVelocity = 10;
    int yVelocity = 0;


    public Stone(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.fireKey = fireKey;
        this.width = width;
        this.height = height;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.enemyImg = new Image(getClass().getResourceAsStream("/Enemy.png"));
        this.imageView = new ImageView(enemyImg);


        this.imageView.setFitWidth(ITEM_WIDTH);
        this.imageView.setFitHeight(ITEM_HEIGHT);
        this.getChildren().addAll(this.imageView);

    }

    public ImageView getImageView() {
        return imageView;
    }


    public void respawn2() {

        x = startX;
        y = startY;
        imageView.setFitWidth(ITEM_WIDTH);
        imageView.setFitHeight(ITEM_HEIGHT);


    }


    public void move(){

        x = x+xVelocity;

    }
    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
            respawn2();
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-ITEM_WIDTH;
            respawn2();
        }
    }

    public void repaint() {
        setTranslateX(x);
        setTranslateY(y);
    }

    public void collided3(Character2 character2) throws InterruptedException {


        //Above c
        if( x == character2.getX()) {

            repaint();
            character2.collapsed();
            character2.respawn();

        }
    }

}
